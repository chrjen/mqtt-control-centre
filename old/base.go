package main

import (
	"fmt"
	"log"

	"github.com/jroimartin/gocui"
)

var splitX = 0
var splitY = 0

var selectedPage = 0
var selectedMax = 1

var menuItems = []string{
	"F1 Subs",
	"F2 Secret",
	"F8 Add",
	"F10 Quit",
}

const menuItemSize = 10
const topicListSize = 10
const dialogSize = 30

var subTopics []string

type MenuWidget struct{}
type SubWidget struct{}
type SecretWidget struct {
	msg string
}

var menuWidget *MenuWidget
var secretWidget *SecretWidget
var subWidget *SubWidget

func (widget *SecretWidget) Layout(gui *gocui.Gui) error {

	maxX, maxY := gui.Size()
	maxY -= 3

	if view, _ := gui.SetView("secret", -1, -1, maxX, 20); view != nil {

		view.Wrap = true
		view.Editable = false
		view.Autoscroll = true

		fmt.Fprintln(view, widget.msg)
	}

	return nil
}

func (widget *SubWidget) Layout(gui *gocui.Gui) error {

	maxX, maxY := gui.Size()
	maxY -= 3

	if view, _ := gui.SetView("topic-list", -1, -1, maxX, topicListSize); view != nil {

		view.Wrap = true
		view.Editable = false
		view.Autoscroll = true

		view.Clear()
		for _, topic := range subTopics {
			fmt.Fprintln(view, topic)
		}
	}

	return nil
}

func (widget *MenuWidget) Layout(gui *gocui.Gui) error {

	maxX, maxY := gui.Size()

	// Draws the background for the menubar at the bottom
	if view, _ := gui.SetView("menubar", -1, maxY-2, maxX, maxY); view != nil {

		if err := gui.SetKeybinding("", gocui.KeyF1, gocui.ModNone, func(gui *gocui.Gui, view *gocui.View) error {
			selectedPage = 0
			return nil
		}); err != nil {
			return err
		}
		if err := gui.SetKeybinding("", gocui.KeyF2, gocui.ModNone, func(gui *gocui.Gui, view *gocui.View) error {
			selectedPage = 1
			return nil
		}); err != nil {
			return err
		}
		if err := gui.SetKeybinding("", gocui.KeyF8, gocui.ModNone, subscribeDialog); err != nil {
			return err
		}
		if err := gui.SetKeybinding("", gocui.KeyF10, gocui.ModNone, quit); err != nil {
			return err
		}
		if err := gui.SetKeybinding("", gocui.KeyTab, gocui.ModNone, func(gui *gocui.Gui, view *gocui.View) error {
			selectedPage = (selectedPage + 1) % selectedMax
			return nil
		}); err != nil {
			return err
		}

		// Mouse input
		if err := gui.SetKeybinding(menuItems[0], gocui.MouseLeft, gocui.ModNone, menuMouseHandler); err != nil {
			return err
		}
		if err := gui.SetKeybinding(menuItems[1], gocui.MouseLeft, gocui.ModNone, menuMouseHandler); err != nil {
			return err
		}
		if err := gui.SetKeybinding("F8 Add", gocui.MouseLeft, gocui.ModNone, subscribeDialog); err != nil {
			return err
		}
		if err := gui.SetKeybinding("F10 Quit", gocui.MouseLeft, gocui.ModNone, quit); err != nil {
			return err
		}

		view.BgColor = gocui.ColorGreen
		view.FgColor = gocui.ColorBlack
		view.Highlight = false
		view.Frame = false
		view.Editable = false

		// Draws the options inside the menu bar
		for i, option := range menuItems {

			if view, err := gui.SetView(option, i*menuItemSize-1, maxY-2, (i+1)*menuItemSize, maxY); err != nil {

				if err != gocui.ErrUnknownView {
					return err
				}

				view.BgColor = gocui.ColorGreen
				view.FgColor = gocui.ColorBlack
				view.Highlight = false
				view.Frame = false
				view.Editable = false
				view.Wrap = false

				if selectedPage == i {
					view.BgColor = gocui.ColorYellow
				}

				fmt.Fprint(view, option)
			}
		}
	}

	return nil
}

func subscribeDialog(gui *gocui.Gui, view *gocui.View) error {

	if view != nil && view.Name() == "sub-input" {

		input := view.Buffer()
		if len(input) > 0 {
			input = input[:len(input)-1]
			subTopics = append(subTopics, input)
		}

		if err := gui.DeleteView("sub-input"); err != nil {
			return err
		}
		return nil
	}

	maxX, maxY := gui.Size()

	if view, err := gui.SetView("sub-input", maxX/2-dialogSize, maxY/2, maxX/2+dialogSize, maxY/2+2); view != nil {

		if err != gocui.ErrUnknownView {
			return err
		}

		view.Editable = true
		view.Wrap = false

		if err := gui.SetKeybinding("sub-input", gocui.KeyEnter, gocui.ModNone, subscribeDialog); err != nil {
			return err
		}

		if _, err := gui.SetCurrentView("sub-input"); err != nil {
			return err
		}
	}

	return nil
}

func menuMouseHandler(gui *gocui.Gui, view *gocui.View) error {

	if view.Name() == menuItems[0] {
		gui.SetManager(menuWidget, subWidget)
	} else if view.Name() == menuItems[1] {
		gui.SetManager(menuWidget, secretWidget)
	}

	return nil
}

func keybindings(gui *gocui.Gui) error {

	return nil
}

func quit(gui *gocui.Gui, view *gocui.View) error {

	return gocui.ErrQuit
}

func main() {

	gui, err := gocui.NewGui(gocui.OutputNormal)

	if err != nil {
		log.Panicln(err)
	}
	defer gui.Close()

	sizeX, sizeY := gui.Size()
	splitX = sizeX / 2
	splitY = sizeY / 2

	gui.Cursor = true
	gui.Mouse = true

	menuWidget = &MenuWidget{}
	subWidget = &SubWidget{}
	secretWidget = &SecretWidget{msg: "THIS IS A SECRET"}

	gui.SetManager(menuWidget, subWidget)

	if err := keybindings(gui); err != nil {
		log.Panicln(err)
	}

	if err := gui.MainLoop(); err != nil && err != gocui.ErrQuit {
		log.Panicln(err)
	}
}
