package main

/*
## Topic format

# Sensor data
Values read should be published using the following format
Payload should ONLY contain numbers formatted as strings e.g. "25.98"
The only exception is "DEAD" which should be published using the devies will.
- type    The kind of sensor being published
- device  Name of the device the sensor is on
- name    The name of the sensor

	/sensor/<type>/<device>/<name>

Types should be one of the following
	- temp (temperature)
	- led (status of leds)
	- button (status of buttons and switches)
	- flame (status of flame sensor)

# Events
Events should follow the following syntax
- type    Name of the event e.g. "flame" for flames detected
- device  Source device the event originates

	/event/<type>/<device>

# Commands
Commands should follow the following syntax
- device  Name of the target device of command
- name    Name of target on the device itself (optional)

	/cmd/<device>/[name]
*/

import (
	"flag"
	"fmt"
	"os"
	"strconv"
	"time"

	"github.com/jroimartin/gocui"

	"github.com/chrjen/mqtt-control-centre/ui"
	"github.com/eclipse/paho.mqtt.golang"
)

var mainGui *gocui.Gui
var client mqtt.Client

var mqttHandler mqtt.MessageHandler = func(client mqtt.Client, msg mqtt.Message) {

	switch msg.Topic() {
	case "/sensor/led/pi/led0":
		mainGui.Update(updateLed(ui.Led0, msg))
	case "/sensor/led/pi/led1":
		mainGui.Update(updateLed(ui.Led1, msg))
	case "/sensor/led/pi/led2":
		mainGui.Update(updateLed(ui.Led2, msg))
	case "/sensor/led/pi/led3":
		mainGui.Update(updateLed(ui.Led3, msg))
	case "/sensor/led/pi/rgb0":
		mainGui.Update(updateLed(ui.Led4, msg))
	case "/sensor/laser/pi/laser0":
		mainGui.Update(updateLed(ui.Led5, msg))
	case "/sensor/button/pi/button0":
		mainGui.Update(updateLed(ui.Button0, msg))
		if string(msg.Payload()) == "1" {
			token := client.Publish("/cmd/pi/buzz", 1, false, "0")
			token.Wait()
		}
	case "/sensor/bright/pi/bright0":
		mainGui.Update(updateBar(ui.Bar0, msg))
	case "/sensor/temp/pi/temp0":
		mainGui.Update(updateValue(ui.Temp0, msg))

	case "/sensor/temp/pi/cpu":
		mainGui.Update(updateValue(ui.Temp1, msg))
	case "/sensor/volt/pi/cpu":
		mainGui.Update(updateValue(ui.Volt0, msg))
	case "/sensor/mem/pi/memory/used":
		mainGui.Update(updateMemory(ui.Mem0, msg))
		mainGui.Update(updateBar(ui.Mem3, msg))
	case "/sensor/mem/pi/memory/free":
		mainGui.Update(updateMemory(ui.Mem1, msg))
	case "/sensor/mem/pi/memory/available":
		mainGui.Update(updateMemory(ui.Mem2, msg))

	case "/event/flame/pi":
		mainGui.Update(func(gui *gocui.Gui) error {
			flames, err := strconv.ParseBool(string(msg.Payload()))
			if err != nil {
				return nil
			}

			ui.FireEvent = flames

			return nil
		})

	case "/event/pi/dead":
		token := client.Publish("/sensor/led/pi/led0", 1, true, "DEAD")
		token.Wait()
		token = client.Publish("/sensor/led/pi/led1", 1, true, "DEAD")
		token.Wait()
		token = client.Publish("/sensor/led/pi/led2", 1, true, "DEAD")
		token.Wait()
		token = client.Publish("/sensor/led/pi/led3", 1, true, "DEAD")
		token.Wait()
		token = client.Publish("/sensor/led/pi/rgb0", 1, true, "DEAD")
		token.Wait()
		token = client.Publish("/sensor/laser/pi/laser0", 1, true, "DEAD")
		token.Wait()
		token = client.Publish("/sensor/button/pi/button0", 1, true, "DEAD")
		token.Wait()
	}
}

func updateLed(led *ui.LedWidget, msg mqtt.Message) func(gui *gocui.Gui) error {

	return func(gui *gocui.Gui) error {
		if string(msg.Payload()) == "DEAD" {
			led.State = ui.GONE
			return nil
		}

		state, err := strconv.Atoi(string(msg.Payload()))
		if err != nil {
			return err
		}

		switch state {
		case 0:
			led.State = ui.OFF
		case 1:
			led.State = ui.ON
		}

		return nil
	}
}

func updateBar(bar *ui.BarWidget, msg mqtt.Message) func(gui *gocui.Gui) error {

	return func(gui *gocui.Gui) error {
		if string(msg.Payload()) == "DEAD" {
			bar.Value = -1
			return nil
		}

		value, err := strconv.Atoi(string(msg.Payload()))
		if err != nil {
			return err
		}

		if value < 0 {
			value = 0
		} else if value > bar.Max {
			value = bar.Max
		}

		bar.Value = value

		return nil
	}
}

func updateValue(vw *ui.ValueWidget, msg mqtt.Message) func(gui *gocui.Gui) error {

	return func(gui *gocui.Gui) error {

		value, err := strconv.ParseFloat(string(msg.Payload()), 32)
		if err != nil {
			return err
		}

		vw.Value = float32(value)

		return nil
	}
}

func updateMemory(vw *ui.ValueWidget, msg mqtt.Message) func(gui *gocui.Gui) error {

	return func(gui *gocui.Gui) error {

		value, err := strconv.Atoi(string(msg.Payload()))
		if err != nil {
			return err
		}

		vw.Value = float32(value) / (1024 * 1024)

		return nil
	}
}

func clickHandler(gui *gocui.Gui, view *gocui.View) error {

	switch view.Name() {
	case ui.Led0.ID:
		var cmd = "ON"
		if ui.Led0.State == ui.ON {
			cmd = "OFF"
		}
		token := client.Publish("/cmd/pi/led0", 1, true, cmd)
		token.Wait()
	case ui.Led1.ID:
		var cmd = "ON"
		if ui.Led1.State == ui.ON {
			cmd = "OFF"
		}
		token := client.Publish("/cmd/pi/led1", 1, true, cmd)
		token.Wait()
	case ui.Led2.ID:
		var cmd = "ON"
		if ui.Led2.State == ui.ON {
			cmd = "OFF"
		}
		token := client.Publish("/cmd/pi/led2", 1, true, cmd)
		token.Wait()
	case ui.Led3.ID:
		var cmd = "ON"
		if ui.Led3.State == ui.ON {
			cmd = "OFF"
		}
		token := client.Publish("/cmd/pi/led3", 1, true, cmd)
		token.Wait()
	case ui.Led4.ID:
		var cmd = "ON"
		if ui.Led4.State == ui.ON {
			cmd = "OFF"
		}
		token := client.Publish("/cmd/pi/rgb0", 1, true, cmd)
		token.Wait()
	case ui.Led5.ID:
		var cmd = "ON"
		if ui.Led5.State == ui.ON {
			cmd = "OFF"
		}
		token := client.Publish("/cmd/pi/laser0", 1, true, cmd)
		token.Wait()
	}

	return nil
}

func main() {
	hostFlag := flag.String("host", "localhost", "The MQTT server to connect to")

	flag.Parse()

	// Channel to return created Gui on
	c := make(chan *gocui.Gui)

	go func() {
		mainGui = <-c
		// fmt.Printf("Connecting to host \"%s\"\n", *hostFlag)
		hosturl := fmt.Sprintf("tcp://%s:1883", *hostFlag)
		opts := mqtt.NewClientOptions().AddBroker(hosturl)
		opts.SetKeepAlive(2 * time.Second)
		opts.SetDefaultPublishHandler(mqttHandler)
		opts.SetPingTimeout(1 * time.Second)

		client = mqtt.NewClient(opts)
		if token := client.Connect(); token.Wait() && token.Error() != nil {
			panic(token.Error())
		}

		if token := client.Subscribe("/sensor/#", 0, nil); token.Wait() && token.Error() != nil {
			fmt.Println(token.Error())
			os.Exit(1)
		}
		if token := client.Subscribe("/event/#", 0, nil); token.Wait() && token.Error() != nil {
			fmt.Println(token.Error())
			os.Exit(1)
		}

	}()

	ui.Loop(c, clickHandler)

	client.Disconnect(250)
}
