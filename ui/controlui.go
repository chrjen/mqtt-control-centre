package ui

import (
	"fmt"
	"log"
	"time"

	"github.com/jroimartin/gocui"
)

// FireEvent is true if there currently is a fire
var FireEvent bool

var flash = false

var fireEventMsg = `!!! Warning !!!
Flames detected`

// LedState represents the state of the attached LEDs
type LedState int

// GONE means LED is disconnected
const GONE = LedState(0)

// ON means LED is emitting light
const ON = LedState(1)

// OFF means LED is not emitting light
const OFF = LedState(2)

/* ARDUINO */
var Led0 = &LedWidget{
	ID:    "led0",
	Title: "Led 0",
	State: GONE,
}

var Led1 = &LedWidget{
	ID:    "led1",
	Title: "Led 1",
	State: GONE,
}

var Led2 = &LedWidget{
	ID:    "led2",
	Title: "Led 2",
	State: GONE,
}

var Led3 = &LedWidget{
	ID:    "led3",
	Title: "Led 3",
	State: GONE,
}

var Led4 = &LedWidget{
	ID:    "led4",
	Title: "RGB",
	State: GONE,
}

var Led5 = &LedWidget{
	ID:    "led5",
	Title: "Laser",
	State: GONE,
}

var Button0 = &LedWidget{
	ID:    "button0",
	Title: "Button",
	State: GONE,
}

var Bar0 = &BarWidget{
	ID:    "bar0",
	Title: "Brightness",
	Max:   1023,
}

var Temp0 = &ValueWidget{
	ID:     "temp0",
	w:      10,
	Title:  "Temp",
	Format: "%5.1f °C",
}

/* RASPBERRY PI */
var Temp1 = &ValueWidget{
	ID:     "temp1",
	w:      11,
	Title:  "CPU temp",
	Format: "%5.1f °C",
}

var Volt0 = &ValueWidget{
	ID:     "volt0",
	w:      11,
	Title:  "CPU volt",
	Format: " %1.4f V",
}

var Mem0 = &ValueWidget{
	ID:     "mem0",
	w:      11,
	Title:  "MEM used",
	Format: " %5.1f MiB",
}

var Mem1 = &ValueWidget{
	ID:     "mem1",
	w:      11,
	Title:  "MEM free",
	Format: " %5.1f MiB",
}

var Mem2 = &ValueWidget{
	ID:     "mem2",
	w:      11,
	Title:  "MEM available",
	Format: " %5.1f MiB",
}

var Mem3 = &BarWidget{
	ID:    "mem3",
	Title: "Memory used [%]",
	Max:   972230656,
}

func layout(gui *gocui.Gui) error {

	maxX, maxY := gui.Size()

	if view, err := gui.SetView("home", -1, 0, maxX, maxY/3); err != nil {
		if err != gocui.ErrUnknownView {
			return err
		}

		view.Title = "Home"
	}

	if view, err := gui.SetView("pi", -1, maxY/3, maxX, 2*maxY/3); err != nil {
		if err != gocui.ErrUnknownView {
			return err
		}

		view.Title = "Raspberry Pi"
	}

	if view, err := gui.SetView("centre", -1, 2*maxY/3, maxX, maxY); err != nil {
		if err != gocui.ErrUnknownView {
			return err
		}

		view.Title = "Control Centre"
	}

	/* ARDUINO */
	Led0.x = 1
	Led0.y = 1
	Led1.x = 10
	Led1.y = 1
	Led2.x = 19
	Led2.y = 1
	Led3.x = 28
	Led3.y = 1
	Button0.x = 37 - 36
	Button0.y = 4
	Led4.x = 46 - 36
	Led4.y = 4
	Temp0.x = 55 - 36
	Temp0.y = 4
	Led5.x = 66 - 36
	Led5.y = 4
	// Line 2
	Bar0.x = 1
	Bar0.y = 7
	Bar0.w = 1.0

	Led0.Layout(gui)
	Led1.Layout(gui)
	Led2.Layout(gui)
	Led3.Layout(gui)
	Led4.Layout(gui)
	Led5.Layout(gui)
	Button0.Layout(gui)
	Bar0.Layout(gui)
	Temp0.Layout(gui)

	/* RASPBERRY PI */
	Temp1.x = 1
	Temp1.y = maxY/3 + 1
	Volt0.x = 13
	Volt0.y = maxY/3 + 1
	Mem0.x = 1
	Mem0.y = maxY/3 + 4
	Mem1.x = 13
	Mem1.y = maxY/3 + 4
	Mem2.x = 25
	Mem2.y = maxY/3 + 4
	Mem3.x = 1
	Mem3.y = maxY/3 + 7
	Mem3.w = 1.0

	Temp1.Layout(gui)
	Volt0.Layout(gui)
	Mem0.Layout(gui)
	Mem1.Layout(gui)
	Mem2.Layout(gui)
	Mem3.Layout(gui)

	if FireEvent {
		x, y := maxX/2-7, maxY/2-1
		var view *gocui.View
		var err error
		if view, err = gui.SetView("fire-warning", x, y, x+16, y+3); err != nil {
			if err != gocui.ErrUnknownView {
				return err
			}

			view.BgColor = gocui.ColorRed
			view.FgColor = gocui.ColorWhite

			fmt.Fprint(view, fireEventMsg)

			go func() {
				for FireEvent {
					flash = !flash
					gui.Update(func(gui *gocui.Gui) error { return nil })
					time.Sleep(800 * time.Millisecond)
				}
			}()
		}

		if flash {
			view.BgColor = gocui.ColorRed
			view.FgColor = gocui.ColorWhite
		} else {
			view.BgColor = gocui.ColorWhite
			view.FgColor = gocui.ColorRed
		}
	} else {
		gui.DeleteView("fire-warning")
	}

	return nil
}

func quit(gui *gocui.Gui, view *gocui.View) error {

	return gocui.ErrQuit
}

// NewUI creates the TUI and returns it
func NewUI() *gocui.Gui {
	gui, err := gocui.NewGui(gocui.OutputNormal)

	if err != nil {
		log.Panicln(err)
	}
	defer gui.Close()

	gui.Cursor = true
	gui.Mouse = true

	gui.SetManagerFunc(layout)

	if err := gui.SetKeybinding("", gocui.KeyCtrlC, gocui.ModNone, quit); err != nil {
		fmt.Println(err)
	}

	return gui
}

// Loop starts the TUI end return when quit happens
func Loop(c chan *gocui.Gui, handler func(*gocui.Gui, *gocui.View) error) {

	gui, err := gocui.NewGui(gocui.OutputNormal)

	if err != nil {
		log.Panicln(err)
	}
	defer gui.Close()

	gui.Cursor = true
	gui.Mouse = true

	gui.SetManagerFunc(layout)

	if err := gui.SetKeybinding("", gocui.KeyCtrlC, gocui.ModNone, quit); err != nil {
		fmt.Println(err)
	}
	if err := gui.SetKeybinding(Led0.ID, gocui.MouseLeft, gocui.ModNone, handler); err != nil {
		fmt.Println(err)
	}
	if err := gui.SetKeybinding(Led1.ID, gocui.MouseLeft, gocui.ModNone, handler); err != nil {
		fmt.Println(err)
	}
	if err := gui.SetKeybinding(Led2.ID, gocui.MouseLeft, gocui.ModNone, handler); err != nil {
		fmt.Println(err)
	}
	if err := gui.SetKeybinding(Led3.ID, gocui.MouseLeft, gocui.ModNone, handler); err != nil {
		fmt.Println(err)
	}
	if err := gui.SetKeybinding(Led4.ID, gocui.MouseLeft, gocui.ModNone, handler); err != nil {
		fmt.Println(err)
	}
	if err := gui.SetKeybinding(Led5.ID, gocui.MouseLeft, gocui.ModNone, handler); err != nil {
		fmt.Println(err)
	}

	c <- gui

	if err := gui.MainLoop(); err != nil && err != gocui.ErrQuit {
		log.Panicln(err)
	}
}
