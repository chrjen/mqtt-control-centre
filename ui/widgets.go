package ui

import (
	"fmt"

	"github.com/jroimartin/gocui"
)

var boxChars = []string{
	" ",
	"▏",
	"▎",
	"▍",
	"▌",
	"▋",
	"▊",
	"▉",
	"█",
}

type LedWidget struct {
	x, y  int
	ID    string
	Title string
	State LedState
}

type BarWidget struct {
	x, y  int
	w     float32
	ID    string
	Title string
	Value int
	Max   int
}

type ValueWidget struct {
	x, y   int
	w      int
	ID     string
	Title  string
	Value  float32
	Format string
}

func (w *ValueWidget) Layout(gui *gocui.Gui) error {

	var view *gocui.View
	var err error

	if view, err = gui.SetView(w.ID, w.x, w.y, w.x+w.w, w.y+2); err != nil {

		if err != gocui.ErrUnknownView {
			return err
		}

		view.Wrap = false
		view.Title = w.Title
	}

	view.Clear()
	fmt.Fprintf(view, w.Format, w.Value)

	return nil
}

func (w *BarWidget) Layout(gui *gocui.Gui) error {

	maxX, _ := gui.Size()

	var view *gocui.View
	var err error

	if view, err = gui.SetView(w.ID, w.x, w.y, w.x+int(float32(maxX-3)*w.w), w.y+2); err != nil {

		if err != gocui.ErrUnknownView {
			return err
		}

		view.Wrap = false
		view.Title = w.Title
	}

	var progressBar string
	view.FgColor = gocui.ColorCyan

	percent := float32(w.Value) / float32(w.Max)
	width, _ := view.Size()
	barSize := percent * float32(width)

	for barSize > 0 {
		if barSize >= 1 {
			progressBar += boxChars[len(boxChars)-1]
		} else {
			progressBar += boxChars[int(float32(len(boxChars))*barSize)]
		}
		barSize -= 1.0
	}

	view.Clear()
	fmt.Fprint(view, progressBar)

	return nil
}

func (w *LedWidget) Layout(gui *gocui.Gui) error {

	var view *gocui.View
	var err error

	if view, err = gui.SetView(w.ID, w.x, w.y, w.x+8, w.y+2); err != nil {

		if err != gocui.ErrUnknownView {
			return err
		}

		view.Wrap = false
		view.Title = w.Title
	}

	var text string
	var bgColour = gocui.ColorDefault
	var fgColour = gocui.ColorDefault

	switch w.State {
	case ON:
		text = "  ON   "
		bgColour = gocui.ColorYellow
		fgColour = gocui.ColorBlack
	case OFF:
		text = "  OFF   "
		bgColour = gocui.ColorRed
		fgColour = gocui.ColorBlack
	case GONE:
		text = "   -    "
		bgColour = gocui.ColorBlack
		fgColour = gocui.ColorWhite
	}

	view.BgColor = bgColour
	view.FgColor = fgColour
	view.Clear()
	fmt.Fprint(view, text)

	return nil
}
